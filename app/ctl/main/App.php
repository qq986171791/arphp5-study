<?php
/**
 * Powerd by ArPHP.
 *
 * default Controller.
 *
 */
namespace app\ctl\main;
use \ar\core\Controller as Controller;

/**
 * Default Controller of webapp.
 */
class App extends Controller
{
    private $utils = [];

    public $version = "";

    public function __construct(){
    }

    //初始化版本
    public function index(){
        $version = \ar\core\get('ver');
        $this->getConfigService()->setVersion($version);
    }
}
