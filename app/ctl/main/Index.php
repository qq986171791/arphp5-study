<?php
/**
 * Powerd by ArPHP.
 *
 * default Controller.
 *
 */
namespace app\ctl\main;
use \ar\core\Controller as Controller;

use \app\lib\model\Article as Article;
/**
 * Default Controller of webapp.
 */
class Index extends Controller
{


    /**
     * just the example of get contents.
     *
     * @return void
     */
    public function index()
    {

    }


}
