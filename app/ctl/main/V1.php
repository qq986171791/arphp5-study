<?php
/**
 * Powerd by ArPHP.
 *
 * default Controller.
 *
 */
namespace app\ctl\main;
use app\ctl\main\App;
/**
 * Default Controller of webapp.
 */
class V1 extends App
{


    /**
     * just the example of get contents.
     *
     * @return void
     */
    public function index()
    {
        $mca = \ar\core\cfg('requestRoute');
        $m = $mca['a_m'];
        $c = $mca['a_c'];
        $a = $mca['a_a'];
        var_dump($mca);
    }

    private $tokenUser = [];
    public function token_user(){
        $requestType = $this->getUtilsService()->getRquestType();
        $this->tokenUser['get'] = function (){
            echo 'get';
        };
        $this->tokenUser['post'] = function (){
            echo 'post';
        };
        $this->tokenUser[$requestType]();
    }
    
}
