<?php
/**
 * Powerd by ArPHP.
 *
 * test service.
 *
 */
namespace app\ctl\main\service;
use \app\lib\model\Article as ArticleModel;
/**
 * Article Controller of webapp.
 */

class Article
{
    private $table = null;
    public function __constructor(){
        $this->table = new ArticleModel();
    }
    public function getList(){
        $this->table->select();
    }
}

