<?php
/**
 * Powerd by ArPHP.
 *
 * test service.
 *
 */
namespace app\ctl\main\service;
/**
 * Article Controller of webapp.
 */

class Login
{
    private $utils = [];

    public function getUsername($username){
        return \ar\core\post('username');
    }
    public function getPassword($password){
        return \ar\core\post('password');
    }
    /*
     * 用户传的是否为账号
     */
    public function isAccount(){
        $username = $this->getUsername();
        $password = $this->getPassword();
        $ret = false;
        if($this->isValid($username) || $this->isValid($password)){
            $ret = true;
        }
        return $ret;
    }

    /*
     * 账号和密码是否有效
     */
    private function isValid($obj){
        $ret = true;
        if($this->isEmpty($obj)){
            $ret = false;
        }
        return $ret;
    }
    /*
     * 是否为空
     */
    private function isEmpty($obj){
        $ret = true;
        if($obj){
           $ret = false;
        }
        return $ret;
    }

}


