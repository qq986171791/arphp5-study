<?php
/**
 * Powerd by ArPHP.
 *
 * test service.
 *
 */
namespace app\ctl\main\service;
/**
 * Article Controller of webapp.
 */
class Utils
{
    //传递数据以易于阅读的样式格式化后输出
    public function p($data){
        // 定义样式
        $str='<pre style="display: block;padding: 9.5px;margin: 44px 0 0 0;font-size: 13px;line-height: 1.42857;color: #333;word-break: break-all;word-wrap: break-word;background-color: #F5F5F5;border: 1px solid #CCC;border-radius: 4px;">';
        // 如果是boolean或者null直接显示文字；否则print
        if (is_bool($data)) {
            $show_data=$data ? 'true' : 'false';
        }elseif (is_null($data)) {
            $show_data='null';
        }else{
            $show_data=print_r($data,true);
        }
        $str.=$show_data;
        $str.='</pre>';
        echo $str;
    }

    /*
     * 判断是否为post请求
     */
    public function isPost(){
        return isset($_SERVER['REQUEST_METHOD']) && strtoupper($_SERVER['REQUEST_METHOD'])=='POST';
    }
    /*
     * 判断是否为get请求
     */
    public function isGet(){
        return isset($_SERVER['REQUEST_METHOD']) && strtoupper($_SERVER['REQUEST_METHOD'])=='GET';
    }
    /*
     * 判断是否为put请求
     */
    public function isPut(){
        return isset($_SERVER['REQUEST_METHOD']) && strtoupper($_SERVER['REQUEST_METHOD'])=='PUT';
    }
    /*
   * 判断是否为patch请求
   */
    public function isPatch(){
        return isset($_SERVER['REQUEST_METHOD']) && strtoupper($_SERVER['REQUEST_METHOD'])=='PATCH';
    }
    /*
    * 判断是否为delete请求
    */
    public function isDelete(){
        return isset($_SERVER['REQUEST_METHOD']) && strtoupper($_SERVER['REQUEST_METHOD'])=='DELETE';
    }
    /*
     * 获取请求类型
     */
    public function getRquestType(){
        if($this->isPost()){
            return 'post';
        }
        if($this->isGet()){
            return 'get';
        }
        if($this->isPut()){
            return 'put';
        }
        if($this->isPatch()){
            return 'patch';
        }
        if($this->isDelete()){
            return 'delete';
        }
    }

}

