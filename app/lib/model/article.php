<?php
namespace app\lib\model;

/**
 * Test model.
 */
class Article extends \ar\core\model
{
    // 表名
    public $tableName = 'article';

    public $db = null;

    function __construct()
    {
        $this->db = $this->getDb();
    }

    //获取文章列表
    public function select($where = []){
        return $this->db->where($where)->queryAll();
    }

}